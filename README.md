Jurassic Park System
---------------------
An attempt to partially re-create a modified version of the
Jurassic Park System developed for InGen company for system
used in Isla Nublar from the fictional novel of Jurassic Park
by Michael Chrichton (which I highly recommend to read for the
technical details over the Jurassic Park film released in 1993).

Goals
------

Unlike in the film of 1993's Jurassic Park and the novel itself, this
system will be developed in Common Lisp. 

This program should be able to run on GNU/Linux system without fail. Also,
we won't write this software and make it as complex as described in the
novel in the term of line of code which the software contains about 1 million
LOC.

This software should cover most programs described in the
novel including gene sequencing repair and matching, power grid controller,
security checker, tour ride system, motion sensoring, general telephone
automation system, park visual monitoring, dinosaur bookeeping visualizer.

Why do you do this?
-------------------
It is fun, :). Well, Jurassic Park movie was my true first
inspiration to be a programmer. I still can't forget the scene when
Ray Arnold (aka John Arnold in the book) trying to fix the computing 
issues with just typing into the console. Not to mention, when Alexis
(or it was Tim in the book) successfully rebooted the whole system.

It was a miracle and the technical details were (and still is)
really fascinating.

Trying it out
-------------
TODO

Known Issues
------------
So far none at this point.

License
-------
In the spirit of Ian Malcom in his state of delirium while under
the influence of morphine, where he rants about complex things
can be almost always difficult to predict the next outcome of and
with his conservative naturalist/humanist standpoint of 
scientifical abuse of ultimate power, this whole project is
released under the AGPL-3.0 license. This project values
you, the user especially, your freedom of computing, and 
this project will never want to control your computing.
Although, maybe the dinosaurs will :-)

Please see LICENSE file for further license details.
