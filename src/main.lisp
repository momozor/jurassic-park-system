;;  For license, see COPYING file for further details.
;;  Copyright (C) 2019  Momozor <skelic3@gmail.com>

(defpackage jurassic-park-system
  (:use :cl)
  (:import-from :cl-interpol
                :enable-interpol-syntax)
  (:import-from :cl-ppcre
                :create-scanner
                :scan)
  (:import-from :cl-ansi-text
                :green
                :red
                :cyan
                :yellow)
  (:export :main))
(in-package :jurassic-park-system)

;; enable stronger type checking in CCL
(declaim (optimize (safety 3)))
(enable-interpol-syntax)

(defparameter +jps-version+ "0.1.0")
(defparameter +system-codename+ "SORNA")

(defun prompt-read (prompt)
  (format *query-io* "~a " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defun prints-system-version (system-version)
  (format t "JURASSIC PARK SYSTEM VERSION: ~a~%" system-version))

(defun boot-preparation ()
  (format t "Preparing.")
  (sleep 1)
  (format t "..")
  (sleep 0.5)
  (format t "...~%~%")
  (sleep 1))

(defun boot-loader ()
  (format t "Booting.")
  (sleep 0.5)
  (format t ".")
  (sleep 0.5)
  (format t "..~%~%")
  (sleep 1))

(defun user-prompt-greet (system-version)
  (format t "~%Welcome to Jurassic Park System: ~a~%" system-version))

(defun user-prompt (system-version)
  (user-prompt-greet system-version))

(defun system-boot-prompt (system-version system-codename)
  (boot-preparation)
  (format t "SYSTEM ARCH: X-CRAY 77482 (SILIGRAPH) 1992~%")
  (sleep 0.8)
  (format t "SYSTEM MEM POOL: 230MB~%")
  (sleep 0.8)
  (format t "SYSTEM VIRTUAL DISK SIZE: 1.8 GB~%")
  (sleep 0.8)
  (format t "SYSTEM CPU: 143 MhZ~%")
  (sleep 0.8)
  (format t "SYSTEM GRAPHICS: TTY/NOX~%~%")
  (sleep 2)
  (prints-system-version system-version)
  (format t "CODENAME: ~a~%~%" system-codename)
  (sleep 0.8)
  (format t "BUFFER POOLING: ")
  (sleep 1)
  (format t (green "OK~%" :effect :bright))
  (sleep 0.3)
  (format t "SECURITY STATUS: ")
  (sleep 1)
  (format t (green "OK~%" :effect :bright))
  (sleep 0.3)
  (format t "NEW PATCH UPDATE: ")
  (sleep 1)
  (format t (cyan "NONE~%" :effect :bright))
  (sleep 0.3)
  (format t "AUTHORIZATION LEVEL: " :effect :bright)
  (sleep 1)
  (format t (green "OK~%~%" :effect :bright))
  (sleep 0.3)
  
  (boot-loader)

  (user-prompt system-version))

(defun get-toplevel-choice (choice)
  (cond ((or (string= choice "quit")
             (string= choice "q")
	     (string= choice "exit"))
         nil)

        ((string= choice "")
         (get-toplevel-choice (prompt-read "$")))

        (t
         (get-toplevel-choice (prompt-read "$")))))

(defun main (&optional
               (system-version +jps-version+)
               (system-codename +system-codename+))
  (system-boot-prompt system-version system-codename)
  (get-toplevel-choice (prompt-read "$")))
