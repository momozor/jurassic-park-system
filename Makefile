PROJECT_NAME=jurassic-park-system
LISP ?= lx86cl64

all:
	$(LISP) --eval "(ql:quickload :$(PROJECT_NAME))" \
                --eval "(asdf:make :$(PROJECT_NAME))" \
				--eval "(uiop:quit)"

