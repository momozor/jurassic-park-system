(defpackage jurassic-park-system/tests/main
  (:use :cl
        :jurassic-park-system
        :rove))
(in-package :jurassic-park-system/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :jurassic-park-system)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
