(defsystem "jurassic-park-system"
    :version "0.1.0"
    :author "Momozor"
    :license "AGPL-3.0"
    :depends-on ("cl-ppcre"
                 "cl-interpol"
                 "cl-strings"
                 "cl-ansi-text")
    :components ((:module "src"
                          :components
                          ((:file "main"))))
    :description "The Jurassic Park System, in Common Lisp"
    :long-description
    #.(read-file-string
       (subpathname *load-pathname* "README.md"))
    :in-order-to ((test-op (test-op "jurassic-park-system/tests")))  :build-operation "program-op" ;; leave as is
    :build-pathname "jps"
    :entry-point "jurassic-park-system::main")

(defsystem "jurassic-park-system/tests"
  :author "Momozor"
  :license "AGPL-3.0"
  :depends-on ("jurassic-park-system"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for jurassic-park-system"

  :perform (test-op (op c) (symbol-call :rove :run c)))
